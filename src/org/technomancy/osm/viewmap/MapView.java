package org.technomancy.osm.viewmap;


import org.andnav.osm.OpenStreetMapActivity;
import org.andnav.osm.R;
import org.andnav.osm.util.TypeConverter;
import org.andnav.osm.util.constants.OpenStreetMapConstants;
import org.andnav.osm.views.OpenStreetMapView;
import org.andnav.osm.views.controller.OpenStreetMapViewController;
import org.andnav.osm.views.overlay.OpenStreetMapViewSimpleLocationOverlay;
import org.andnav.osm.views.util.OpenStreetMapRendererInfo;
import org.andnav.osm.util.GeoPoint;

import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.location.LocationManager;

public class MapView extends OpenStreetMapActivity implements OpenStreetMapConstants {
	// ===========================================================
	// Constants
	// ===========================================================
	
	private static final int MENU_ZOOMIN_ID = Menu.FIRST;
	private static final int MENU_ZOOMOUT_ID = MENU_ZOOMIN_ID + 1;
	private static final int MENU_RENDERER_ID = MENU_ZOOMOUT_ID + 1;
	private static final int MENU_MY_LOCATION_ID = MENU_RENDERER_ID + 1;

	// ===========================================================
	// Fields
	// ===========================================================

    private OpenStreetMapView mOsmv;
	private OpenStreetMapViewSimpleLocationOverlay mMyLocationOverlay; 

	protected GeoPoint mLocation;

	// ===========================================================
	// Constructors
	// ===========================================================
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, false); // Pass true here to actually contribute to OSM!
        
        final RelativeLayout rl = new RelativeLayout(this);
        
        this.mOsmv = new OpenStreetMapView(this, OpenStreetMapRendererInfo.MAPNIK);

        // Default of 0 looks silly when fully zoomed out.
        this.mOsmv.setZoomLevel(1);
        
        rl.addView(this.mOsmv, new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        
        /* SingleLocation-Overlay */
        {
	        /* Create a static Overlay showing a single location. (Gets updated in onLocationChanged(Location loc)! */
	        this.mMyLocationOverlay = new OpenStreetMapViewSimpleLocationOverlay(this);
	        this.mOsmv.getOverlays().add(mMyLocationOverlay);
        }
        
        /* ZoomControls */
        {
	        /* Create a ImageView with a zoomIn-Icon. */
	        final ImageView ivZoomIn = new ImageView(this);
	        ivZoomIn.setImageResource(R.drawable.zoom_in);
	        /* Create RelativeLayoutParams, that position in in the top right corner. */
	        final RelativeLayout.LayoutParams zoominParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
	        zoominParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	        zoominParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
	        rl.addView(ivZoomIn, zoominParams);
	        
	        ivZoomIn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					MapView.this.mOsmv.zoomIn();
				}
	        });
	        
	        
	        /* Create a ImageView with a zoomOut-Icon. */
	        final ImageView ivZoomOut = new ImageView(this);
	        ivZoomOut.setImageResource(R.drawable.zoom_out);
	        
	        /* Create RelativeLayoutParams, that position in in the top left corner. */
	        final RelativeLayout.LayoutParams zoomoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
	        zoomoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
	        zoomoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
	        rl.addView(ivZoomOut, zoomoutParams);
	        
	        ivZoomOut.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					MapView.this.mOsmv.zoomOut();
				}
	        });
        }
        
        this.setContentView(rl);
    }

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods from SuperClass/Interfaces
	// ===========================================================
    
	@Override
	public void onLocationChanged(final Location pLoc) {
        this.mLocation = TypeConverter.locationToGeoPoint(pLoc);
		this.mMyLocationOverlay.setLocation(this.mLocation);
	}

	@Override
	public void onLocationLost() {
		this.mLocation = null;
	}
	
    
    @Override
	public boolean onCreateOptionsMenu(final Menu pMenu) {
    	pMenu.add(0, MENU_ZOOMIN_ID, Menu.NONE, "Zoom In");
    	pMenu.add(0, MENU_ZOOMOUT_ID, Menu.NONE, "Zoom Out");
    	
    	final SubMenu subMenu = pMenu.addSubMenu(0, MENU_RENDERER_ID, Menu.NONE, "Map Style");
    	{
	    	for(int i = 0; i < OpenStreetMapRendererInfo.values().length; i ++)
	    		subMenu.add(0, 1000 + i, Menu.NONE, OpenStreetMapRendererInfo.values()[i].NAME);
    	}
        

    	pMenu.add(1, MENU_MY_LOCATION_ID, Menu.NONE, "My Location");
    	
    	return true;
	}

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        // The only item in group 1 is the My Location button
        // if we have an location lock then we enable this button.
        menu.setGroupEnabled(1, this.mLocation != null);
        return true;
    }

    
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()){
			case MENU_ZOOMIN_ID:
				this.mOsmv.zoomIn();
				return true;
				
			case MENU_ZOOMOUT_ID:
				this.mOsmv.zoomOut();
				return true;
				
			case MENU_RENDERER_ID:
				this.mOsmv.invalidate();
				return true;

			case MENU_MY_LOCATION_ID:
                this.mOsmv.setMapCenter(this.mLocation);
				this.mOsmv.setZoomLevel(15);
				return true;
				
			default: 
				this.mOsmv.setRenderer(OpenStreetMapRendererInfo.values()[item.getItemId() - 1000]);
		}
		return false;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
